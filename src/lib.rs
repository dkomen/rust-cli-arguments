//! An easy-to-use and command line arguments processor for Rust
//!
//! # Examples
//!
//! - myfile -h
//! - myfile --h
//! - myfile -a -l -v or myfile -alv
//! - myfile -a --display
//! - myfile -i input_file_name -o output_file_name
use std::env;

/// Represents a specific commmand-line argument and its value
#[derive(Clone, Debug)]
pub struct Argument{
    pub name: String,
    pub value: String,
    pub validation_regex: String,
    pub validation_failed_message: Option<String>
}
impl Argument {
    /// Pre-register an allowed command-line argument and its validation reg-ex string
    pub fn register(name: &str, default_value: &str, validation_regex: &str, validation_failed_message: Option<String>) -> Argument {
        Argument{
            name: name.into(),
            value: default_value.into(),
            validation_regex: validation_regex.into(),
            validation_failed_message
        }
    }

    /// Set the arguments value with the actual command-line value
    pub fn set_value(&mut self, value: &str) -> Result<Argument, Option<String>> {
        self.value = value.into();
        let result_of_validation = self.validate_value();
        if result_of_validation.is_ok() {
            Ok(
                Argument{
                    name: self.name.clone(),
                    value: value.into(),
                    validation_regex: self.validation_regex.clone(),
                    validation_failed_message: self.validation_failed_message.clone()
                }
            )
        } else {
            Err(result_of_validation.unwrap_err())
        }
    }

    /// Do a reg-ex validation
    pub fn validate_value(&self) -> Result<(), Option<String>> {
        let regex_validator = regex::Regex::new(&self.validation_regex);

        if regex_validator.unwrap().is_match(&self.value) || self.validation_regex.len()==0 {
            Ok(()) 
        } else {
            Err(self.validation_failed_message.clone())
        }
    }
}

#[derive(Clone, Debug)]
pub struct Arguments {
    list: Vec<Argument>
}
impl Arguments {

    pub fn new() -> Arguments {
        Arguments {
            list: vec![]
        }
    }

    /// Get arguments from command-line
    pub fn get_from_command_line(&mut self) -> Result<Arguments, Vec<String>> {
        let args_array: Vec<String> = env::args().collect::<Vec<String>>();
        let result_arguments  = self.get_arguments(args_array);

        if result_arguments.is_ok() {
            Ok(
                Arguments {
                    list: result_arguments.unwrap()
                }
            )
        } else {
            Err(result_arguments.unwrap_err())
        }
    }

    /// Check that the arguments received are in the expected layout
    fn check_arguments(args_array: &Vec<String>) -> Result<(), String> {
        let mut next_must_be_argument = true;
        for i in 1..args_array.len() {
            if next_must_be_argument && !args_array[i].starts_with("-") {
                return Err("Invalid command line arguments".into());
            } else {
                if args_array[i].starts_with("-") {
                    next_must_be_argument = false;
                } else {
                    next_must_be_argument = !next_must_be_argument;
                }
            }
        }

        Ok(())
    }

    /// If an argument exists on the command line then add it and its value to Argument Vec to return
    fn get_arguments(&mut self, cli_args_array: Vec<String>) -> Result<Vec<Argument>, Vec<String>> {    
        if cli_args_array.len() == 1 {
            return Ok(vec![])
        }

        let result_get_args = Arguments::check_arguments(&cli_args_array);
        if result_get_args.is_ok() {
            let result_args_array = self.extract_arguments_and_values(&cli_args_array);

            result_args_array
        } else {
            let errors: Vec<String> = vec![result_get_args.unwrap_err()];
            Err(errors)
        }
    }

    /// Pre-register an allowed command-line argument and its validation reg-ex string
    pub fn register(&mut self, name: &str, default_value: &str, validation_regex: &str, validation_failed_message: Option<String>) -> Argument {
        let registered_argument = Argument::register(name, default_value, validation_regex, validation_failed_message);
        self.list.push(registered_argument.clone());

        registered_argument
    }

    /// Find the argument in the list of allowed arguments and set its value, if its not found then add it
    fn set_value(&mut self, name: &str, value: &str) -> Result<Argument, Option<String>> {
        for argument in &mut self.list {
            if argument.name == name {
                return argument.set_value(value);
            }
        }
        
        let registered_argument = self.register(name, value, "", None);

        Ok(registered_argument)
    }

    ///Extract all the individual arguments and their values if they have any
    fn extract_arguments_and_values(&mut self, cli_args_array: &Vec<String>) -> Result<Vec<Argument>, Vec<String>> {        
        let mut errors_array: Vec<String> = vec![];
        let mut arg_index: usize = 0;
        for _ in 0..cli_args_array.len(){
            if cli_args_array[arg_index].starts_with('-') {
                let value: String = match arg_index < cli_args_array.len()-1 { //Get the argument value (if a value actually exists)
                    true => match !cli_args_array[arg_index+1].starts_with('-') {
                                true => cli_args_array[arg_index+1].clone(), //next argument contains the value
                                _ => "".into() //no value argument found
                            },
                    _ => "".into() //no value argument found
                };

                if cli_args_array[arg_index].starts_with("--") {
                    let result_argument = self.set_value(&cli_args_array[arg_index], &value);
                    if result_argument.is_ok() {
                        arg_index+=1;
                    } else {
                        let error_message = result_argument.unwrap_err();
                        if error_message.is_some() {                        
                            errors_array.push(error_message.unwrap());
                        }
                    }
                } else {
                    if cli_args_array[arg_index].len() > 2 { // eg: -au == 3
                        //Extract all the concatenated arguments
                        let sub_args: &str = cli_args_array[arg_index].strip_prefix('-').unwrap();
                        //sub_args.as_bytes().to_vec().iter().for_each(|b|args_array.push(Argument::new(&format!("-{}", char::from(*b)),"")));
                        sub_args.as_bytes().to_vec().iter().for_each(|argument| {
                                let result_argument = self.set_value(&format!("-{}", char::from(*argument)),"");
                                if result_argument.is_err() {
                                    let error_message = result_argument.unwrap_err();
                                    if error_message.is_some() {                        
                                        errors_array.push(error_message.unwrap());
                                    }
                                }
                            }
                        );                        
                    } else {
                        let result_argument = self.set_value(&cli_args_array[arg_index], &value);
                        if result_argument.is_err() {
                            let error_message = result_argument.unwrap_err();
                            if error_message.is_some() {                        
                                errors_array.push(error_message.unwrap());
                            }
                        }
                        //args_array.push(Argument::new(&cli_args_array[arg_index], &value));
                    }
                }
            }
            arg_index+=1;
        }

        if errors_array.len() > 0 {
            Err(errors_array)
        } else {
            Ok(self.list.clone())
        }
    }

    

    /// Set an arguments value
    pub fn set_argument_value(&mut self, argument_name: &str, new_value: &str) -> Result<(), Option<String>> {
        let option_argument = self.list.iter_mut().find(|arg| arg.name == argument_name);
        if option_argument.is_some() { //Argument was found
            let updated_argument = option_argument.unwrap();
            let result = updated_argument.set_value(new_value);
            if result.is_ok() {
                //self.list.retain(|arg| arg.name != String::from(argument_name));

                //self.list.push(updated_argument.clone());
                return Ok(());
            } else {
                return Err(result.unwrap_err());
            }
        } else {
            Err(Some(format!("Argument '{}' was not found", argument_name)))
        }
    }

    /// Get the value of an argument if it was supplied on the command line
    pub fn get_argument_value(&self, argument_name: &str) -> Option<String> {
        for argument in &self.list {
            if argument.name == argument_name {
                return Some(argument.value.to_owned());
            }
        };
        None
    }
}